// ------------------------------------------------------------------------------------------------
// Copyright (c) EasyStep2 BV. All rights reserved. 
// Licensed under the MIT License. See License.txt in the project root for license information.
// ------------------------------------------------------------------------------------------------

/// <summary> 
/// Enumeration determining the provider for barcode encoding module. 
/// Microsoft uses the 1D-fonts from ID-Automation to generate the barcodes. 
/// 
/// Values can be either:
/// <param name="Default">Default Microsoft Provider</param>
/// 
/// or can be extended  with your own providers
////// </summary>
enumextension 50100 myProviders extends BarcodeProviders
{
    /// <summary>
    /// uses the open source BWIP barcode provider.
    /// </summary>
    value(11255664; BWIP)
    {
        Caption = 'BWIP Engine';
        Implementation = IBarcodeProvider = BWIPBarcodeProvider;
    }
}

