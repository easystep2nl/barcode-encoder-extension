// ------------------------------------------------------------------------------------------------
// Copyright (c) EasyStep2 BV. All rights reserved. 
// Licensed under the MIT License. See License.txt in the project root for license information.
// ------------------------------------------------------------------------------------------------

/// <summary> 
/// Enumeration determining the option for encoding modules for over 100 different barcode types and standards. 
/// All linear and two-dimensional barcodes in common use (and many uncommon ones) are available. 
/// Values can be either:
/// <remarks>Point of sale</remarks>
/// <param name="ean13">EAN-13</param>
/// <param name="ean8">EAN-8</param>
/// <param name="upca">UPC-A</param>
/// <param name="upce">UPC-E</param>
/// <param name="isbn">ISBN</param>
/// <param name="ismn">ISMN</param>
/// <param name="issn">ISSN</param>
/// 
/// <remarks>Two-Dimensional</remarks>
/// <param name="azteccode">Aztec Code</param>
/// <param name="aztecrune">Aztec Runes</param>
/// <param name="datamatrix">Data Matrix</param>
/// <param name="datamatrixrectangular">Data Matrix Rectangular</param>
/// <param name="datamatrixrectangularextension">Data Matrix Rectangular Extension</param>
/// <param name="hanxin">Han Xin Code</param>
/// <param name="micropdf417">MicroPDF417</param>
/// <param name="pdf417">PDF417</param>
/// <param name="pdf417compact">Compact PDF417</param>
/// <param name="qrcode">QR Code</param>
/// <param name="microqrcode">Micro QR Code</param>
/// 
/// <remarks>One-dimensional</remarks>
/// <param name="code128">Code 128</param>
/// <param name="code39">Code 39</param>
/// <param name="code39ext">Code 39 Extended</param>
/// <param name="code93">Code 93</param>
/// <param name="code93ext">Code 93 Extended</param>
/// <param name="interleaved2of5">Interleaved 2 of 5 (ITF)</param>
/// 
/// <remarks>Supply Chain</remarks>
/// <param name="gs1datamatrix">GS1 Data Matrix</param> 
/// <param name="gs1qrcode">GS1 QR Code</param>
/// <param name="gs1-128">GS1-128</param>
/// <param name="ean14">GS1-14</param>
/// <param name="itf14">ITF-14</param>
/// <param name="sscc18">SSCC-18</param>
/// <param name="databaromni">GS1 DataBar Omnidirectional</param>
/// <param name="databarstackedomni">GS1 DataBar Stacked Omnidirectional</param>
/// <param name="databarexpanded">GS1 DataBar Expanded</param>
/// <param name="databarexpandedstacked">GS1 DataBar Expanded Stacked</param>
/// <param name="databartruncated">GS1 DataBar Truncated</param>
/// <param name="databarstacked">GS1 DataBar Stacked</param>
/// <param name="databarlimited">GS1 DataBar Limited</param>
/// 
/// <remarks>GS1 Composite Symbols</remarks>
/// <param name="ean13composite">EAN-13 Composite</param>
/// <param name="ean8composite">EAN-8 Composite</param>
/// <param name="upcacomposite">UPC-A Composite</param>
/// <param name="upcecomposite">UPC-E Composite</param>
/// <param name="databaromnicomposite">GS1 DataBar Omnidirectional Composite</param>
/// <param name="databarstackedomnicomposite">GS1 DataBar Stacked Omnidirectional Composite</param>
/// <param name="databarexpandedcomposite">GS1 DataBar Expanded Composite</param>
/// <param name="databarexpandedstackedcomposite">GS1 DataBar Expanded Stacked Composite</param>
/// <param name="databartruncatedcomposite">GS1 DataBar Truncated Composite</param>
/// <param name="databarstackedcomposite">GS1 DataBar Stacked Composite</param>
/// <param name="databarlimitedcomposite">GS1 DataBar Limited Composite</param>
/// <param name="gs1-128composite">GS1-128 Composite</param>
/// <param name="gs1-cc">GS1 Composite 2D Component</param>
/// 
/// <remarks>Postal Symbols</remarks>
/// <param name="gs1northamericancoupon">GS1 North American Coupon</param>
/// <param name="auspost">AusPost 4 State Customer Code</param>
/// <param name="identcode">Deutsche Post Identcode</param>
/// <param name="leitcode">Deutsche Post Leitcode</param>
/// <param name="japanpost">Japan Post 4 State Customer Code</param>
/// <param name="maxicode">MaxiCode</param>/// 
/// <param name="royalmail">Royal Mail 4 State Customer Code</param>
/// <param name="mailmark">Royal Mail Mailmark</param> 
/// <param name="kix">Royal Dutch TPG Post KIX</param>
/// <param name="onecode">USPS Intelligent Mail</param>
/// <param name="postnet">USPS POSTNET</param>
/// <param name="planet">USPS PLANET</param>
/// <param name="symbol">Miscellaneous symbols</param>
///  
/// <remarks>Pharmaceutical Symbols</remarks>
/// <param name="code32">Italian Pharmacode</param>
/// <param name="pharmacode">Pharmaceutical Binary Code</param>
/// <param name="pharmacode2">Two-track Pharmacode</param>
/// <param name="pzn">Pharmazentralnummer (PZN)</param>
/// <param name="hibccode39">HIBC Code 39</param>
/// <param name="hibccode128">HIBC Code 128</param>
/// <param name="hibcazteccode">HIBC Aztec Code</param>
/// <param name="hibcpdf417">HIBC PDF417</param>
/// <param name="hibcmicropdf417">HIBC MicroPDF417</param>
/// <param name="hibcqrcode">HIBC QR Code</param>
/// <param name="hibcdatamatrix">HIBC Data Matrix</param>
/// <param name="hibccodablockf">HIBC Codablock F</param>
///
/// <remarks>Less-used Symbols</remarks>
/// <param name="bc412">BC412</param>
/// <param name="channelcode">Channel Code</param>
/// <param name="rationalizedCodabar">Codabar</param>
/// <param name="codablockf">Codablock F</param>
/// <param name="code11">Code 11</param>
/// <param name="code16k">Code 16K</param>
/// <param name="code2of5">Code 25</param>
/// <param name="dotcode">DotCode</param>
/// <param name="ultracode">Ultracode</param>
/// <param name="iata2of5">IATA 2 of 5</param>
/// <param name="codeone">Code One</param>
/// <param name="code49">Code 49</param>
/// <param name="coop2of5">COOP 2 of 5</param>
/// <param name="industrial2of5">Industrial 2 of 5</param>
/// <param name="matrix2of5">Matrix 2 of 5</param>
/// <param name="msi">MSI Modified Plessey</param>
/// <param name="plessey">Plessey UK</param>
/// <param name="posicode">PosiCode</param>
/// <param name="telepen">Telepen</param>
/// <param name="telepennumeric">Telepen Numeric</param>
/// 
/// <remarks>Raw Symbols</remarks>
/// <param name="daft">Custom 4 state symbology</param>
/// <param name="flattermarken">Flattermarken</param>
/// <param name="raw">Custom 1D symbology</param>
/// 
/// <remarks>Partial Symbols</remarks>
/// <param name="ean2">EAN-2 (2 digit addon)</param>
/// <param name="ean5">EAN-5 (5 digit addon)</param>
/// </summary>
enumextension 50101 myBarcodeEncoding extends BarcodeSymbology
{
    //#Region One-Dimensional
    value(11255664; BWIPdatamatrix)
    {
        Caption = 'Datamatrix (BWIP)';
        Implementation = IBarcodeEncoder = BWIPDatamatrixEncoder;
    }
}
