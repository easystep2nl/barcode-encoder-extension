// ------------------------------------------------------------------------------------------------
// Copyright (c) EasyStep2 BV. All rights reserved. 
// Licensed under the MIT License. See License.txt in the project root for license information.
// ------------------------------------------------------------------------------------------------
codeunit 50101 BWIPBarcodeProvider implements IBarcodeProvider
{
    Access = Public;

    /// <summary> 
    /// Function to determine the correct Barcode Encoding Handler based at the Enum.
    /// </summary>
    /// <seealso cref="OnFindBarcodeEncodingHandler"/>
    /// <param name="iBarcodeEncodingHandler">Interface object IBarcodeEncodingHandler to connect the requested Encoding Handler.</param>
    /// <param name="UseEncoding">Enum BarcodeEncoding of the reqested Barcode Encoding Handler.</param>
    /// <summary> 
    /// Function to determine the correct Barcode Encoding Handler based at the Enum.
    /// </summary>
    /// <seealso cref="OnFindBarcodeEncodingHandler"/>
    /// <param name="iBarcodeEncodingHandler">Interface object IBarcodeEncodingHandler to connect the requested Encoding Handler.</param>
    /// <param name="UseEncoding">Enum BarcodeEncoding of the reqested Barcode Encoding Handler.</param>
    procedure GetBarcodeEncoder(var iBarcodeEncoder: interface IBarcodeEncoder; UseSymbology: Enum BarcodeSymbology)
    var
        CannotFindBarcodeEncoderErr: label 'Error finding a barcode encoder for %1';
    begin
        if TryGetBarcodeEncoder(iBarcodeEncoder, UseSymbology) then
            OnFindBarcodeEncodingHandler(iBarcodeEncoder, UseSymbology)
        else
            error(GetLastErrorText);
    end;

    procedure GetListofImplementedEncoders(var ListOfEncoders: list of [Text])
    var
        EnumSymbololy: Enum BarcodeSymbology;
    begin
        // Include List of encoders implemented with this provider
        clear(ListOfEncoders);
        ListOfEncoders.add(format(EnumSymbololy::code39));
        ListOfEncoders.add(format(EnumSymbololy::BWIPdatamatrix));
    end;

    [TryFunction]
    local procedure TryGetBarcodeEncoder(var iBarcodeEncoder: interface IBarcodeEncoder; UseSymbology: Enum BarcodeSymbology)
    var
        ListOfEncoders: List of [text];
        CannotFindBarcodeEncoderErr: label 'Error finding a barcode encoder for %1';
    begin
        // Find which standard encoding handler to use
        GetListofImplementedEncoders(ListOfEncoders);
        If ListOfEncoders.Contains(format(UseSymbology)) then
            iBarcodeEncoder := UseSymbology
        else
            Error(CannotFindBarcodeEncoderErr, UseSymbology)
    end;

    /// <summary> 
    /// Integration event, emitted from <see cref="GetBarcodeEncodingHandler"/>.
    /// Subscribe to this event to change the default behavior by changing the provided parameter(s).
    /// </summary>
    /// <seealso cref="GetBarcodeEncodingHandler"/>
    /// <param name="var iBarcodeEncodingHandler">Parameter of type interface IBarcodeEncodingHandler.</param>
    /// <param name="UseEncoding">Parameter of type enum BarcodeEncoding.</param>
    [IntegrationEvent(false, false)]
    local procedure OnFindBarcodeEncodingHandler(var iBarcodeEncoder: interface IBarcodeEncoder; UseSymbology: Enum BarcodeSymbology);
    begin
    end;
}