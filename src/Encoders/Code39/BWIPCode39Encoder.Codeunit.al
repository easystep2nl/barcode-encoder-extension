// ------------------------------------------------------------------------------------------------
// Copyright (c) EasyStep2 BV. All rights reserved. 
// Licensed under the MIT License. See License.txt in the project root for license information.
// ------------------------------------------------------------------------------------------------
codeunit 50102 BWIPCode39BarcodeEncoder
{
    Access = Internal;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::Code39BarcodeEncoder, 'OnBeforeEncodeCode39', '', true, true)]
    local procedure OnBeforeEncodeCode39(var BarcodeEncoding: Record BarcodeEncoding temporary; var IsHandled: Boolean)
    var
        EncoderImpl: Codeunit BWIPCode39EncoderImpl;
    begin
        EncoderImpl.Encode(BarcodeEncoding, IsHandled);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::Code39BarcodeEncoder, 'OnBeforeValidateCode39', '', true, true)]
    local procedure OnBeforeValidateCode39(var BarcodeEncoding: Record BarcodeEncoding temporary; var IsHandled: Boolean)
    var
        EncoderImpl: Codeunit BWIPCode39EncoderImpl;
    begin
        EncoderImpl.Validate(BarcodeEncoding, IsHandled);
    end;

    [EventSubscriber(ObjectType::Codeunit, Codeunit::Code39BarcodeEncoder, 'OnBeforeFormatCode39', '', true, true)]
    local procedure OnBeforeFormatCode39(var BarcodeEncoding: Record BarcodeEncoding temporary; var IsHandled: Boolean)
    var
        EncoderImpl: Codeunit BWIPCode39EncoderImpl;
    begin
        EncoderImpl.Format(BarcodeEncoding, IsHandled);
    end;
}