// ------------------------------------------------------------------------------------------------
// Copyright (c) EasyStep2 BV. All rights reserved. 
// Licensed under the MIT License. See License.txt in the project root for license information.
// ------------------------------------------------------------------------------------------------

/// <summary> 
/// EAN-13 Barcode Encoder and is used to create an EAN-13 barcode, 
/// which is most commonly used to encode 13 digits of the GTIN barcode symbology.
/// </summary>
codeunit 50110 BWIPDatamatrixEncoder implements IBarcodeEncoder
{
    Access = Internal;

    /// <summary> 
    /// Function to encode a EAN-13 barcode and returns an Media image
    /// <seealso cref="OnBeforeEncodeDatamatrix"/> and <seealso cref="OnAfterEncodeDatamatrix"/> 
    /// </summary>
    /// <param name="BarcodeEncoding">Parameter of type Record BarcodeEncoding defining the options for the barcode to create.</param>
    procedure Encode(var BarcodeEncoding: Record BarcodeEncoding)
    var
        DatamatrixEncoderImpl: Codeunit BWIPDatamatrixEncoderImpl;
        IsHandled: Boolean;
    begin
        OnBeforeEncodeDatamatrix(BarcodeEncoding, IsHandled);

        DatamatrixEncoderImpl.Encode(BarcodeEncoding, IsHandled);

        OnAfterEncodeDatamatrix(BarcodeEncoding);
    end;

    /// <summary> 
    /// Function to Validate a EAN-13 barcode and returns an error if the options and inputstring are not according to the requirements
    /// <seealso cref="OnBeforeValidateDatamatrix"/> and <seealso cref="OnAfterValidateDatamatrix"/> 
    /// </summary>
    /// <param name="BarcodeEncoding">Parameter of type Record BarcodeEncoding defining the options for the barcode to create.</param>
    procedure Validate(var BarcodeEncoding: Record BarcodeEncoding) Result: Boolean
    var
        DatamatrixEncoderImpl: Codeunit BWIPDatamatrixEncoderImpl;
        IsHandled: Boolean;
    begin
        OnBeforeValidateDatamatrix(BarcodeEncoding, IsHandled);

        DatamatrixEncoderImpl.Validate(BarcodeEncoding, Result, IsHandled);

        OnAfterValidateDatamatrix(BarcodeEncoding);
    end;

    /// <summary> 
    /// Function to format the inputstring of a EAN-13 barcode
    /// <seealso cref="OnBeforeFormatDatamatrix"/> and <seealso cref="OnAfterFormatDatamatrix"/> 
    /// </summary>
    /// <param name="BarcodeEncoding">Parameter of type Record BarcodeEncoding defining the options for the barcode to create.</param>
    procedure Format(var BarcodeEncoding: Record BarcodeEncoding)
    var
        DatamatrixEncoderImpl: Codeunit BWIPDatamatrixEncoderImpl;
        IsHandled: Boolean;
    begin
        OnBeforeFormatDatamatrix(BarcodeEncoding, IsHandled);

        DatamatrixEncoderImpl.Format(BarcodeEncoding, IsHandled);

        OnAfterFormatDatamatrix(BarcodeEncoding);
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeEncodeDatamatrix(var BarcodeEncoding: Record BarcodeEncoding temporary; var IsHandled: Boolean)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterEncodeDatamatrix(var BarcodeEncoding: Record BarcodeEncoding temporary)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeValidateDatamatrix(var BarcodeEncoding: Record BarcodeEncoding temporary; var IsHandled: Boolean)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterValidateDatamatrix(var BarcodeEncoding: Record BarcodeEncoding temporary)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnBeforeFormatDatamatrix(var BarcodeEncoding: Record BarcodeEncoding temporary; var IsHandled: Boolean)
    begin
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterFormatDatamatrix(var BarcodeEncoding: Record BarcodeEncoding temporary)
    begin
    end;
}
