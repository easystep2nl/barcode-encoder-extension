// ------------------------------------------------------------------------------------------------
// Copyright (c) EasyStep2 BV. All rights reserved. 
// Licensed under the MIT License. See License.txt in the project root for license information.
// ------------------------------------------------------------------------------------------------
codeunit 50111 BWIPDatamatrixEncoderImpl
{
    Access = Internal;

    var
        WebCallFailedErrorMsg: label 'The call to the web service failed and returned message:\\Status code: %1\\Description: %2';
        WebCallBlockedBySystemErrorMsg: label 'The call to the web service is blocked by the enviroment.';


    // Overloading Encode Function from default system Encoding Setup
    procedure Encode(var BarcodeEncoding: Record BarcodeEncoding temporary; IsHandled: Boolean)
    var
        BarcodeParamString: text;
        EncodingLbl: label 'datamatrix', locked = true;
    begin

        if IsHandled then exit;

        with BarcodeEncoding do begin

            // Isolate the execution only for this provider
            if Provider <> Provider::BWIP then
                exit;

            BarcodeParamString := GetBaseUrl();

            // Set the Format and Encoding Options of barcode
            BarcodeParamString += StrSubstNo('?bcid=%1&text=%2', EncodingLbl, "Input String");

            // Set Scale of the Barcode
            BarcodeParamString += StrSubstNo('&width=%1&height=%2&scale=%3', Width, Height, 2); // Default Scale =2

            // Show Alternative text below barcode
            if (IncludeText) then begin
                BarcodeParamString += StrSubstNo('&includetext&alttext=%1', "Input String"); // Add alternative text
                BarcodeParamString += StrSubstNo('&textcolor=%1', '000000'); // Color = black
                                                                             //   BarcodeParamString += StrSubstNo('&textxalign=center&textxoffset=%1', Margin);
            end;

            // Set background color
            BarcodeParamString += StrSubstNo('&backgroundcolor=%1', 'ffffff'); // White

            // Set barcode color
            BarcodeParamString += StrSubstNo('&barcolor=%1', '000000'); // Black

            // Set show border and bordercolor
            //BarcodeOptionString += StrSubstNo('&showborder&bordercolor=%1', '000000'); // Black

            // Set Rotation of bar code
            // N : Normal (not rotated). This is the default. 
            // R : Clockwise 90 degree rotation. 
            // L : Counter-clockwise 90 degree rotation. 
            // I : Inverted 180 degree rotation.
            BarcodeParamString += StrSubstNo('&rotate=%1', 'N');

            // And Append Additional Options
            //BarcodeParamString += StrSubstNo('&%1', BarcodeOptionString);

            // Init API call
            if not CallWebService(BarcodeEncoding, BarcodeParamString) then
                exit;
        end;
    end;

    // Validate the Inputstring of the barcode
    procedure Validate(var BarcodeEncoding: Record BarcodeEncoding temporary; var Result: Boolean; IsHandled: Boolean)
    begin
        if IsHandled then exit;

        Result := true;
    end;

    // Format the Inputstring of the barcode
    procedure Format(var BarcodeEncoding: Record BarcodeEncoding temporary; IsHandled: Boolean)
    begin
        with BarcodeEncoding do begin
            // Isolate the execution only for this provider
            if Provider <> Provider::BWIP then
                exit;

            // Do my formatting 
        end;
    end;

    local procedure CallWebService(var Barcode: Record BarcodeEncoding; BarcodeParamString: Text) Success: Boolean
    var
        Client: HttpClient;
        AuthHeaderValue: HttpHeaders;
        Headers: HttpHeaders;
        RequestMessage: HttpRequestMessage;
        ResponseMessage: HttpResponseMessage;
        ResponseContent: HttpContent;
        TempBlob: Codeunit "Temp Blob";
        InStr: InStream;
        ErrorMessage: Text;
    begin
        with Barcode do begin
            RequestMessage.Method := 'get';
            RequestMessage.SetRequestUri(BarcodeParamString);
            if not Client.Send(RequestMessage, ResponseMessage) then
                // Get errormessage when failed
                error(WebCallFailedErrorMsg,
                  ResponseMessage.HttpStatusCode,
                  ResponseMessage.ReasonPhrase);

            // Verify if the web service call is blocked by the envirment (BC Cloud version/Sandbox aka is Extension allowed to send Outbound Http Requests)
            If ResponseMessage.IsBlockedByEnvironment then
                error(WebCallBlockedBySystemErrorMsg);

            // Send Response Content into Media
            // Verify Response Type content-Type=PNG -> OK Content-Type=Text -> Error Message
            ResponseContent := ResponseMessage.Content;
            if ResponseMessage.IsSuccessStatusCode then begin
                TempBlob.CreateInStream(InStr);
                ResponseContent.ReadAs(InStr);
                Clear("Barcode Image");
                "Barcode Image".ImportStream(InStr, "Input String", 'image/gif');
            end
            else begin
                ResponseContent.ReadAs(ErrorMessage);
                error(ErrorMessage);
            end;

            exit(ResponseMessage.IsSuccessStatusCode);
        end;
    end;

    local procedure GetBaseUrl(): Text
    begin
        // Temp Open Source later own hosted Web App or Azure Function !!!!
        // ID Automation Webservice https://www.idautomation.com/hosted-saas-barcode-generator/
        // Javascript with App Service GitHub:: https://github.com/metafloor/bwip-js.git
        // Azure functions + Dotnet.core GitHub:: https://github.com/trackabout/barcode-image-service
        exit('http://bwipjs-api.metafloor.com/');
    end;
}
